# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.0

- minor: Add UPLOAD_SOURCEMAP_PATH parameters

## 0.9.0

- minor: Add UPLOAD_SOURCEMAP_PATH parameters

## 0.8.0

- minor: Add UPLOAD_SOURCEMAP_PATH parameters

## 0.7.0

- minor: Add UPLOAD_SOURCEMAP_PATH parameters

## 0.6.0

- minor: Add UPLOAD_SOURCEMAP_PATH parameters

## 0.5.0

- minor: Add ENVIRONMENT and FINALIZE parameters

## 0.4.0

- minor: Add ENVIRONMENT and FINALIZE parameters

## 0.3.0

- minor: Add ENVIRONMENT and FINALIZE parameters

## 0.2.6

- patch: Update pipe.yml

## 0.2.5

- patch: no :

## 0.2.4

- patch: add debug SENTRY_LOG_LEVEL
- patch: bump pipe.sh

## 0.2.3

- patch: Update version again

## 0.2.2

- patch: Update version

## 0.2.1

- patch: fix version number

## 0.2.0

- minor: Add SENTRY_PIPELINE env var
- patch: Update path to pipe.sh

## 0.1.1

- patch: Initial version update

## 0.1.0

- minor: First minor version

## 0.0.7

- patch: no tabs only spaces

## 0.0.6

- patch: reformat readme again

## 0.0.5

- patch: fix readme formatting

## 0.0.4

- patch: fix readme

## 0.0.3

- patch: add icon

## 0.0.2

- patch: Testing release

## 0.0.1

- patch: Initial release

